"use strict";

modulum('MyButtonEditWidget', ['ButtonEditWidget', 'WidgetFactory'],
  function(context, cls) {

    cls.MyButtonEditWidget = context.oo.Class(cls.ButtonEditWidget, function($super) {
      return {
        __name: "MyButtonEditWidget",

        /* your custom code */
      };
    });
    cls.WidgetFactory.register('ButtonEdit', cls.MyButtonEditWidget);
  });
