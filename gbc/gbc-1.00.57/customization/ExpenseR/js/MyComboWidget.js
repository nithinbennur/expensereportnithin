"use strict";

modulum('MyComboWidget', ['ComboBoxWidget', 'WidgetFactory'],
  function(context, cls) {

    cls.MyComboWidget = context.oo.Class(cls.ComboBoxWidget, function($super) {
      return {
        __name: "MyComboWidget",

        /* your custom code */
      };
    });
    cls.WidgetFactory.register('ComboBox', cls.MyComboWidget);
  });
