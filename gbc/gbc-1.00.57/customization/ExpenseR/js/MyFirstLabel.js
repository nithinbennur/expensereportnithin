"use strict";

modulum('MyFirstLabel', ['LabelWidget', 'WidgetFactory'],
  function(context, cls) {

    cls.MyFirstLabel = context.oo.Class(cls.LabelWidget, function($super) {
      return {
        __name: "MyFirstLabel",

        /* your custom code */
      };
    });
    cls.WidgetFactory.register('Label', cls.MyFirstLabel);
  });