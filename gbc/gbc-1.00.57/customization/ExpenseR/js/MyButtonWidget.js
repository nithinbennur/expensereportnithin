"use strict";

modulum('MyButtonWidget', ['ButtonWidget', 'WidgetFactory'],
  function(context, cls) {

    cls.MyButtonWidget = context.oo.Class(cls.ButtonWidget, function($super) {
      return {
        __name: "MyButtonWidget",

        /* your custom code */
      };
    });
    cls.WidgetFactory.register('Button', cls.MyButtonWidget);
  });
