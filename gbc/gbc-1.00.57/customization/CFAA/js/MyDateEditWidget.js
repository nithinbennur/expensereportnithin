"use strict";

modulum('MyDateEditWidget', ['DateEditWidget', 'WidgetFactory'],
  function(context, cls) {

    cls.MyDateEditWidget = context.oo.Class(cls.DateEditWidget, function($super) {
      return {
        __name: "MyDateEditWidget",

        /* your custom code */
      };
    });
    cls.WidgetFactory.register('DateEdit', cls.MyDateEditWidget);
  });
