"use strict";

modulum('MyEditWidget', ['EditWidget', 'WidgetFactory'],
  function(context, cls) {

    cls.MyEditWidget = context.oo.Class(cls.EditWidget, function($super) {
      return {
        __name: "MyEditWidget",

        /* your custom code */
      };
    });
    cls.WidgetFactory.register('Edit',cls.MyEditWidget);
  });