# Here I am importing some functionality from other section
IMPORT util
IMPORT FGL zoomw

# For Database Connection 
DATABASE expensedb

# Here are the global variables defined section and which i used in this file

GLOBALS 
    DEFINE
    emp_rec RECORD
        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,
       -- emp_no1 LIKE emp_mast.emp_no,
        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department
    END RECORD,
    arr_expense DYNAMIC ARRAY OF RECORD
        serial_no LIKE exp_details.serial_no,
        Exp_ID LIKE exp_details.Exp_ID,
        date LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        miles_total DECIMAL(9,2),
        total_charged_to_fourjs DECIMAL(9,2),
        total_paid_by_employee DECIMAL(9,2)
    END RECORD
    TYPE busi RECORD
        serial_no LIKE business_details.serial_no,
        exp_id LIKE business_details.exp_id,
        date LIKE business_details.date,
        amount LIKE business_details.amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans
        
    END RECORD
DEFINE busn DYNAMIC ARRAY OF busi
DEFINE today_date DATE
DEFINE rpt VARCHAR(255)
DEFINE handl om.SaxDocumentHandler
DEFINE totalp DECIMAL(9,2)
DEFINE totalc DECIMAL(9,2)
END GLOBALS 

# These are the constants values defined which can be used anywhere in the program.

CONSTANT msg02 = "Enter Search Criteria"
CONSTANT msg03 = "Canceled By User"
CONSTANT msg04 = "No Rows Found in Table"
CONSTANT msg05 = "End Of List"
CONSTANT msg06 = "Begin Of List"

CONSTANT msg08 = "Row Added"
CONSTANT msg09 = "Row Updated"
CONSTANT msg10 = "Row Deleted"
CONSTANT msg11 = "Enter Employee"
CONSTANT msg12 = "This Employee Does Not Exists"
CONSTANT msg13 = "Bussiness Updated"
CONSTANT C_TAX = 0.575


# Here in this Mainwindow User to enter details of Employee  
# User Can Query Information 
# User Can Print the Report
MAIN 
    DEFINE ha_employee, query_ok SMALLINT
    DEFER INTERRUPT
  
    CLOSE WINDOW SCREEN
    OPEN WINDOW Emp WITH FORM "mainform"
    MENU
        ON ACTION NEW
            CLEAR FORM
            LET query_ok = FALSE
            CALL close_emp()
            LET ha_employee = emp_new()
            IF ha_employee THEN
                CALL arr_expense.clear()
                CALL expense_inpupd()
                CALL input_buss()
            END IF
        ON ACTION Find
            CLEAR FORM
            LET query_ok = employee_query()
            LET ha_employee = query_ok
        ON ACTION NEXT
            CALL emp_fetch_rel(1)
        ON ACTION PREVIOUS
            CALL emp_fetch_rel(-1)
            CLEAR FORM
        ON ACTION print 
          CALL print_fun()
        ON ACTION EXIT
            EXIT MENU
    END MENU
    CLOSE WINDOW Emp
END MAIN 

# User Can Add New Data What he/she Spent his expense
# By Using Zoom Window User can take Employee Basic Information

FUNCTION emp_new()
    DEFINE
        id INTEGER,
        name STRING,
        position STRING,
        manager STRING,
        department STRING
    MESSAGE msg11

    INITIALIZE emp_rec.* TO NULL
    SELECT MAX(exp_id) + 1 INTO emp_rec.exp_id FROM exp_header
    IF emp_rec.exp_id IS NULL OR emp_rec.exp_id == 0 THEN
        LET emp_rec.exp_id = 100
    END IF
    
    LET int_flag = FALSE
    INPUT BY NAME emp_rec.exp_id,
        emp_rec.emp_no,
        emp_rec.notes,
        emp_rec.charge_type,
        emp_rec.emp_name,
        emp_rec.position,
        emp_rec.manager,
        emp_rec.department
        WITHOUT DEFAULTS
        ATTRIBUTES(UNBUFFERED)

        
        ON CHANGE emp_no
            SELECT * INTO emp_rec.* FROM exp_header WHERE emp_no = emp_rec.emp_no
            IF (SQLCA.SQLCODE == NOTFOUND) THEN
                ERROR msg12
                NEXT FIELD emp_no
            END IF

        AFTER FIELD notes
            IF emp_rec.notes IS NULL THEN 
                ERROR "notes should not null"
                NEXT FIELD notes
            END IF 
            IF NOT emp_rec.notes MATCHES "[a-zA-Z]*" THEN 
                ERROR "notes should not be null"
                NEXT FIELD notes
            END IF 

        ON ACTION zoom
            CALL display_emplist(
                ) RETURNING id, NAME, position, manager, department

            IF (id > 0) THEN
                LET emp_rec.emp_no = id
                LET emp_rec.emp_name = NAME
                LET emp_rec.position = position
                LET emp_rec.manager = manager
                LET emp_rec.department = department

            END IF

    END INPUT

    IF (int_flag) THEN
        LET int_flag = FALSE
        CLEAR FORM
        MESSAGE msg03
        RETURN FALSE
    END IF

    RETURN employee_insert()

END FUNCTION

# User Can Insert Employee Information Into Header Table

FUNCTION employee_insert()

    --DISPLAY BY NAME emp_rec.*
    WHENEVER ERROR CONTINUE
    INSERT INTO exp_header(
        exp_id,
        emp_no,
        notes,
        charge_type)
        VALUES(emp_rec.exp_id,
            emp_rec.emp_no,
            emp_rec.notes,
            emp_rec.charge_type)
    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE <> 0) THEN
        CLEAR FORM
        ERROR SQLERRMESSAGE
        RETURN FALSE
    END IF

    MESSAGE "Employee Added-----Now You Add Expense Detail"
    RETURN TRUE

END FUNCTION

# User Can Query Employee Information From DB and Return the selected Information

FUNCTION employee_query()
    DEFINE where_clause STRING
    MESSAGE msg02

    LET int_flag = FALSE
    CONSTRUCT BY NAME where_clause
        ON exp_header.exp_id,
            exp_header.emp_no,
            exp_header.notes,
            exp_header.charge_type

    IF (int_flag) THEN
        LET int_flag = FALSE
        CLEAR FORM
        MESSAGE msg03
        RETURN FALSE
    END IF

    RETURN employee_select(where_clause)

END FUNCTION

# Here in this function By using select statement, selecting employee info 
# And comparing this with Expense ID and Employee Number

FUNCTION employee_select(where_clause)
    DEFINE
        where_clause STRING,
        sql_text STRING

    LET sql_text =
       "SELECT "
|| "exp_header.exp_id,"
|| "exp_header.emp_no, "
|| "exp_header.notes, "
|| "exp_header.charge_type, "
|| "emp_mast.emp_name, "
|| "emp_mast.position, "
|| "emp_mast.manager, "
|| "emp_mast.department "
|| "FROM exp_header, emp_mast, exp_details "
|| "WHERE exp_header.exp_id = exp_details.exp_id and emp_mast.emp_no = exp_header.emp_no "
|| "AND "
|| where_clause

    DECLARE emp_curs SCROLL CURSOR FROM sql_text
    OPEN emp_curs
    IF (NOT emp_fetch(1)) THEN
        CLEAR FORM
        MESSAGE msg04
        RETURN FALSE
    END IF

    RETURN TRUE

END FUNCTION

# Fetching Employee Info
# Calling Expense Details Function and Business Details Function

FUNCTION emp_fetch(p_fetch_flag)
    DEFINE p_fetch_flag SMALLINT

    IF p_fetch_flag = 1 THEN
        FETCH NEXT emp_curs INTO emp_rec.*
    ELSE
        FETCH PREVIOUS emp_curs INTO emp_rec.*
    END IF

    IF (SQLCA.SQLCODE == NOTFOUND) THEN
        RETURN FALSE
    END IF

    DISPLAY BY NAME emp_rec.*
    CALL expense_fetch()
    CALL Bussiness_fetch()
    RETURN TRUE

END FUNCTION

# Closing the Employee Cursor 

FUNCTION close_emp()
    WHENEVER ERROR CONTINUE
    CLOSE emp_curs
    WHENEVER ERROR STOP
END FUNCTION

# Here In This if not of employee flag it will show message

FUNCTION emp_fetch_rel(p_fetch_flag)
    DEFINE p_fetch_flag SMALLINT

    MESSAGE " "
    IF (NOT emp_fetch(p_fetch_flag)) THEN
        IF (p_fetch_flag = 1) THEN
            MESSAGE msg05
        ELSE
            MESSAGE msg06
        END IF
    END IF

END FUNCTION

# Here in this , Selecting Expense Details using expense id

FUNCTION expense_fetch()
    DEFINE
        expense_cnt INTEGER,
        item_rec RECORD
            serial_no LIKE exp_details.serial_no,
            Exp_ID LIKE exp_details.Exp_ID,
            DATE LIKE exp_details.date,
            location LIKE exp_details.location,
            expense_type LIKE exp_details.expense_type,
            mileage LIKE exp_details.mileage,
            amount LIKE exp_details.amount,
            charge_to_fourjs LIKE exp_details.charge_to_fourjs,
            miles_total DECIMAL(9,2),
            total_charged_to_fourjs DECIMAL(9,2),
            total_paid_by_employee DECIMAL(9,2)
        END RECORD

    IF emp_rec.exp_id IS NULL THEN
        RETURN
    END IF

    DECLARE exp_curs CURSOR FOR
        SELECT exp_details.serial_no,
            exp_details.Exp_ID,
            exp_details.date,
            exp_details.location,
            exp_details.expense_type,
            exp_details.mileage,
            exp_details.amount,
            exp_details.charge_to_fourjs,
            exp_details.mileage * C_TAX miles_total,
            exp_details.amount + exp_details.charge_to_fourjs total_charged_to_fourjs,
           -- exp_details.amount total_paid_by_employee,
            exp_details.mileage * C_TAX total_paid_by_employee,
            exp_details.mileage * C_TAX total_charged_to_fourjs
            FROM exp_details
            WHERE exp_details.Exp_ID = emp_rec.exp_id

    LET expense_cnt = 0
    CALL arr_expense.clear()
    FOREACH exp_curs INTO item_rec.*
        LET expense_cnt = expense_cnt + 1
        LET arr_expense[expense_cnt].* = item_rec.*
    END FOREACH
    FREE exp_curs

    CALL expenseData_show()

END FUNCTION

# Here Business details Fetching using expense id

FUNCTION Bussiness_fetch()
    DEFINE
        bussiness_cnt INTEGER,
        bus_rec RECORD
            serial_no LIKE business_details.serial_no,
            exp_id LIKE business_details.exp_id,
            date LIKE business_details.date,
            amount LIKE business_details.amount,
            company_name LIKE business_details.company_name,
            bussiness_trans LIKE business_details.business_trans
            
        END RECORD

    IF bus_rec.exp_id IS NULL THEN
        RETURN
    END IF

    DECLARE buss_curs CURSOR FOR
        SELECT business_details.serial_no,
            business_details.exp_id,
            business_details.date,
            business_details.amount,
            business_details.company_name,
            business_details.business_trans
            
            FROM business_details
            WHERE business_details.Exp_ID = emp_rec.exp_id

    LET bussiness_cnt = 0
    CALL busn.clear()
    FOREACH buss_curs INTO bus_rec.*
        LET bussiness_cnt = bussiness_cnt + 1
        LET busn[bussiness_cnt].* = bus_rec.*
    END FOREACH
    FREE buss_curs

    CALL bussData_show()

END FUNCTION

# Using Screen record, Display will happen

FUNCTION bussData_show()
    DISPLAY ARRAY busn TO sa_buss.*
        BEFORE DISPLAY
            EXIT DISPLAY
    END DISPLAY
END FUNCTION

# Using Screen record, Display will happen

FUNCTION expenseData_show()
    DISPLAY ARRAY arr_expense TO sa_exp.*
        BEFORE DISPLAY
            EXIT DISPLAY
    END DISPLAY
END FUNCTION

# In this For Inputing Expense Input from Employee
# If User selects Mileage ,amount field should be hidden
# All the calculation will happen here
# On row change for Current row updation
# Delete also employee can do

FUNCTION expense_inpupd()
    DEFINE
        opflag CHAR(1),
        expense_cnt, curr_pa SMALLINT

    LET opflag = "U"

    LET expense_cnt = arr_expense.getLength()

    INPUT ARRAY arr_expense
        WITHOUT DEFAULTS
        FROM sa_exp.*
        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
        BEFORE INPUT

        BEFORE ROW
            LET curr_pa = ARR_CURR()
            LET arr_expense[curr_pa].mileage = 0.575
            LET opflag = "U"

        BEFORE INSERT
            LET opflag = "I"
            SELECT MAX(serial_no) + 1
                INTO arr_expense[curr_pa].serial_no
                FROM exp_details
            IF arr_expense[curr_pa].serial_no IS NULL
                OR arr_expense[curr_pa].serial_no == 0 THEN
                LET arr_expense[curr_pa].serial_no = 1
            END IF
            LET arr_expense[curr_pa].Exp_ID = emp_rec.exp_id
            LET arr_expense[curr_pa].amount = 0
            LET arr_expense[curr_pa].date = TODAY
            -- LET arr_expense[curr_pa].total_by_fourjs = 0
            --      LET c_tax = 0.575

        AFTER INSERT
            CALL expense_insert(curr_pa)

        BEFORE DELETE
            CALL expense_delete(curr_pa)

        ON ROW CHANGE
            CALL expense_update(curr_pa)

        BEFORE FIELD Exp_ID
            IF opflag = "U" THEN
                NEXT FIELD DATE
            END IF
        AFTER FIELD location 
              IF arr_expense[arr_curr()].location IS NULL THEN 
                MESSAGE "Enter Location"
                NEXT FIELD location 
              END IF 
              IF NOT arr_expense[arr_curr()].location MATCHES "[a-zA-Z]*" THEN 
                MESSAGE "Enter Location Correctly"
                NEXT FIELD location
            END IF 
        AFTER FIELD expense_type
            IF (arr_expense[arr_curr()].expense_type = 105) THEN
                MESSAGE "Choose one expense_type"
                CALL DIALOG.setFieldActive("amount", 0)
                NEXT FIELD mileage
            ELSE
                CALL DIALOG.setFieldActive("amount", 1)
                NEXT FIELD amount
                
            END IF
            IF (arr_expense[arr_curr()].expense_type = 111)  THEN 
                CALL DIALOG.setFieldActive("mileage",0)
                NEXT FIELD amount
                LET arr_expense[arr_curr()].total_charged_to_fourjs = arr_expense[arr_curr()].amount + arr_expense[arr_curr()].charge_to_fourjs
                LET arr_expense[arr_curr()].total_paid_by_employee = 0
            ELSE
                CALL DIALOG.setFieldActive("amount", 1)
                NEXT FIELD amount
            END IF 

            -- NEXT FIELD charged_to_fourjs
            --AFTER FIELD milage
            --IF arr_expense[arr_curr()].milage IS NULL THEN
            --    MESSAGE "Please enter quantity"
            --    NEXT FIELD milage
            --END IF

--             ON CHANGE amount
--                  WHENEVER ERROR CONTINUE
--                  LET arr_expense[arr_curr()].total_charged_to_fourjs = arr_expense[arr_curr()].amount + arr_expense[arr_curr()].charged_to_fourjs
--
--                  LET arr_expense[arr_curr()].total_charged_to_fourjs = arr_expense[arr_curr()].total_paid_by_employee
--                  WHENEVER ERROR STOP
--                  IF (SQLCA.SQLCODE = 0)THEN
--                     CALL DIALOG.setFieldActive("milage",0)
--                    END IF
        ON CHANGE mileage
            WHENEVER ERROR CONTINUE
            LET arr_expense[arr_curr()].miles_total =
                C_TAX * arr_expense[arr_curr()].mileage

            WHENEVER ERROR STOP
            IF (SQLCA.SQLCODE = 0) THEN
                DISPLAY BY NAME arr_expense[arr_curr()].*
                --CALL DIALOG.setFieldActive("amount",FALSE)
            END IF
            -- NEXT FIELD amount
            --AFTER FIELD amount
            --  IF NOT arr_expense[arr_curr()].expense_type = 105 THEN
            --    MESSAGE "Amount"
            --    NEXT FIELD amount
            --    END IF
        AFTER FIELD charge_to_fourjs
            IF arr_expense[arr_curr()].charge_to_fourjs IS NULL THEN
                MESSAGE "You must enter a value"
                NEXT FIELD charge_to_fourjs
            END IF

            
            LET arr_expense[arr_curr()].total_charged_to_fourjs =
                arr_expense[arr_curr()].amount
                    + arr_expense[arr_curr()].charge_to_fourjs
            DISPLAY "Total Charged ",
                arr_expense[arr_curr()].total_charged_to_fourjs

            
            LET arr_expense[arr_curr()].total_paid_by_employee =
                arr_expense[arr_curr()].miles_total
        
            IF NOT arr_expense[arr_curr()].expense_type = 105 THEN
                LET arr_expense[arr_curr()].total_paid_by_employee =
                    arr_expense[arr_curr()].amount
                DISPLAY "Total Paid ",
                    arr_expense[arr_curr()].total_paid_by_employee
                
            END IF
            IF arr_expense[arr_curr()].expense_type = 105 THEN 
                LET arr_expense[arr_curr()].total_paid_by_employee = arr_expense[arr_curr()].miles_total
                LET arr_expense[arr_curr()].total_charged_to_fourjs =
                arr_expense[arr_curr()].miles_total
            END IF 

    END INPUT

    LET expense_cnt = arr_expense.getLength()

    IF (int_flag) THEN
        LET int_flag = FALSE
    END IF

END FUNCTION

# Initializer function for loading combobox

FUNCTION fill_data(cmb)

    DEFINE cmb ui.ComboBox
    CALL cmb.addItem(101, "Entertainment")
    CALL cmb.addItem(102, "Meals")
    CALL cmb.addItem(103, "Lodging")
    CALL cmb.addItem(104, "Auto Rent")
    CALL cmb.addItem(105, "Miles")
    CALL cmb.addItem(106, "Fuel")
    CALL cmb.addItem(107, "Transport")
    CALL cmb.addItem(108, "Tolls/PK")
    CALL cmb.addItem(109, "Phone")
    CALL cmb.addItem(110, "Others")
    CALL cmb.addItem(111, "Airfare")

END FUNCTION

# For Inserting Expense Input 
# Also Assigning same Expense id for multi row addition 

FUNCTION expense_insert(curr_pa)
    DEFINE curr_pa SMALLINT

    LET curr_pa = arr_curr()

    WHENEVER ERROR CONTINUE
    LET arr_expense[curr_pa].Exp_ID = emp_rec.Exp_ID
    INSERT INTO exp_details(
        serial_no,
        Exp_ID,
        DATE,
        location,
        expense_type,
        Mileage,
        amount,
        charge_to_fourjs)
        VALUES(arr_expense[curr_pa].serial_no,
            arr_expense[curr_pa].Exp_ID,
            arr_expense[curr_pa].date,
            arr_expense[curr_pa].location,
            arr_expense[curr_pa].expense_type,
            arr_expense[curr_pa].mileage,
            arr_expense[curr_pa].amount,
            arr_expense[curr_pa].charge_to_fourjs)
    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg08
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

# Employee can update row by row updation (current row)

FUNCTION expense_update(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    UPDATE exp_details
        SET date = arr_expense[curr_pa].date,
            location = arr_expense[curr_pa].location,
            expense_type = arr_expense[curr_pa].expense_type,
            mileage = arr_expense[curr_pa].mileage,
            amount = arr_expense[curr_pa].amount,
            charge_to_fourjs = arr_expense[curr_pa].charge_to_fourjs
        
        WHERE exp_details.serial_no = arr_expense[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg09
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

# User can delete current row deletion using serial no key

FUNCTION expense_delete(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    DELETE FROM exp_details
        WHERE exp_details.serial_no = arr_expense[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg10
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

# For entering Business info , On row change for Current row updation, on delete for current row deletion

FUNCTION input_buss()
    DEFINE
        opflag CHAR(1),
        buss_cnt, curr_pa1 SMALLINT

    LET opflag = "U"

    LET buss_cnt = busn.getLength()

    INPUT ARRAY busn
        WITHOUT DEFAULTS
        FROM sa_buss.*
        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
        BEFORE INPUT

        BEFORE ROW
            LET curr_pa1 = ARR_CURR()
            -- LET arr_expense[curr_pa1].milage = 0.575
            LET opflag = "U"

        BEFORE INSERT
            LET opflag = "I"
            SELECT MAX(serial_no) + 1
                INTO busn[curr_pa1].serial_no
                FROM business_details
            IF busn[curr_pa1].serial_no IS NULL
                OR busn[curr_pa1].serial_no == 0 THEN
                LET busn[curr_pa1].serial_no = 1
            END IF
            LET busn[curr_pa1].exp_ID = emp_rec.exp_id
            --LET arr_expense[curr_pa1].amount = 0
            LET busn[curr_pa1].date = TODAY
            -- LET arr_expense[curr_pa].total_by_fourjs = 0
            --      LET c_tax = 0.575

        AFTER INSERT
            CALL insert_buss(curr_pa1)

        BEFORE DELETE
            CALL buss_delete(curr_pa1)

        ON ROW CHANGE
            CALL buss_update(curr_pa1)

        BEFORE FIELD date
            IF opflag = "U" THEN
                NEXT FIELD amount
            END IF
        AFTER FIELD company_name
            IF busn[curr_pa1].company_name IS NULL THEN
                MESSAGE "Enter Company Name"
                --CALL DIALOG.setFieldActive("amount",0)
                NEXT FIELD company_name

            END IF

    END INPUT

    LET buss_cnt = busn.getLength()

    IF (int_flag) THEN
        LET int_flag = FALSE
    END IF

END FUNCTION

# For Inserting Business Info

FUNCTION insert_buss(g)
    DEFINE g SMALLINT
    LET g = arr_curr()
    LET busn[g].exp_id = arr_expense[g].Exp_ID
    WHENEVER ERROR CONTINUE
    INSERT INTO business_details VALUES(busn[g].*)

    WHENEVER ERROR STOP
    -- LET cnt = cnt + 1
    IF SQLCA.SQLCODE = 0 THEN
        MESSAGE "Row Added"
    ELSE
        ERROR SQLERRMESSAGE
    END IF
END FUNCTION

# For Deletion of Business Info Using Serial No

FUNCTION buss_delete(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    DELETE FROM business_details
        WHERE business_details.serial_no = busn[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg10
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

# For Business Upadtion Using Serial No

FUNCTION buss_update(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    UPDATE business_details
        SET serial_no = busn[curr_pa].serial_no,
            date = busn[curr_pa].date,
            amount = busn[curr_pa].amount,
            company_name = busn[curr_pa].company_name,
            business_trans = busn[curr_pa].business_trans,
            exp_id = busn[curr_pa].exp_id
        WHERE business_details.serial_no = busn[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg13
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION display_bus()
    DEFINE d INT
    LET d = 1
    DISPLAY BY NAME busn[d].*
END FUNCTION

# For Report Printing Function 
# Defined Functional variables used in the function

FUNCTION print_fun()
     DEFINE rec RECORD
        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,
        -- expense_id1 LIKE employee.expense_id,
        date LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        miles_total DECIMAL(9,2),
        total_charged_to_fourjs DECIMAL(9,2),
        total_paid_by_employee DECIMAL(9,2),

        date1 LIKE business_details.date,
        amount1 LIKE business_details.amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans,
        Entertainment VARCHAR(50),
        Meals VARCHAR(50),
        Lodging VARCHAR(50),
        Autorent VARCHAR(50),
        Fuel VARCHAR(50),
        Transport VARCHAR(50),
        Tolls VARCHAR(50),
        Miles VARCHAR(50),
        Phone VARCHAR(50),
        Others VARCHAR(50),
        Airfare VARCHAR(50)
    --expense_id LIKE bussiness_table.expense_id

    END RECORD

    IF fgl_report_loadCurrentSettings("reporte.4rp") THEN
        CALL fgl_report_selectDevice("PDF")

        CALL fgl_report_selectPreview(TRUE)
        LET handl = fgl_report_commitCurrentSettings()
    ELSE
        EXIT PROGRAM
    END IF

    START REPORT Report_Emp TO XML HANDLER handl

    DECLARE curs123 CURSOR FOR
        SELECT exp_header.exp_id,
            exp_header.emp_no,
            emp_mast.emp_name,
            emp_mast.position,
            emp_mast.manager,
            emp_mast.department,
            exp_header.notes,
            exp_header.charge_type,
            exp_details.date,
            exp_details.location,
            exp_details.expense_type,
            exp_details.mileage,
            exp_details.amount,
            exp_details.charge_to_fourjs,
            exp_details.mileage * C_TAX miles_total,
            exp_details.amount + exp_details.charge_to_fourjs total_charged_to_fourjs,
            exp_details.amount total_paid_by_employee,
           -- exp_details.mileage * C_TAX total_paid_by_employee,
            business_details.date,
            business_details.amount,
            business_details.company_name,
            business_details.business_trans
            FROM exp_header, emp_mast, exp_details, business_details
            WHERE exp_details.exp_id = emp_rec.exp_id
            AND exp_header.exp_id = emp_rec.exp_id AND emp_mast.emp_no = emp_rec.emp_no
            GROUP BY exp_details.serial_no
    --    INNER
    --    JOIN expense_details
    --    ON employee.expense_id = expense_details.expense_id
    ----INNER
    ----JOIN bussiness_table
    ----ON expense_details.expense_id = bussiness_table.expense_id
    --    AND employee.expense_id = emp_rec.expense_id
    -- GROUP BY employee.expense_id
    LET totalp = 0
    LET totalc = 0
    FOREACH curs123 INTO rec.*
        IF rec.expense_type = 101 THEN 
            LET rec.Entertainment = rec.amount
        END IF 
        IF rec.expense_type = 102 THEN
            LET rec.Meals = rec.amount
        END IF 
        IF rec.expense_type = 103 THEN 
            LET rec.Lodging = rec.amount
        END IF 
        IF rec.expense_type = 104 THEN 
            LET rec.Autorent = rec.amount
        END IF 
        IF rec.expense_type = 105 THEN 
            LET rec.Miles = rec.amount
            LET rec.total_paid_by_employee = rec.miles_total
        END IF
        IF rec.expense_type = 106 THEN 
            LET rec.Fuel = rec.amount
        END IF 
        IF rec.expense_type = 107 THEN 
            LET rec.Transport = rec.amount
        END IF 
        IF rec.expense_type = 108 THEN 
            LET rec.Tolls = rec.amount
        END IF 
        IF rec.expense_type = 109 THEN 
            LET rec.Phone = rec.amount
        END IF 
        IF rec.expense_type = 110 THEN 
            LET rec.Others = rec.amount
        END IF 
        IF rec.expense_type = 111 THEN 
            LET rec.Airfare = rec.amount
            LET rec.total_paid_by_employee = 0
        END IF 
        --IF rec.charge_type = "Due Employee" THEN 
        --    LET rec.charge_type = "Y"
        --ELSE 
        --    LET rec.charge_type = "N"
        --END IF 
        LET totalp = totalp + rec.total_paid_by_employee
        LET totalc = totalc + rec.total_charged_to_fourjs
        OUTPUT TO REPORT Report_Emp(rec.*)
    END FOREACH

    FINISH REPORT Report_Emp
END FUNCTION 

# Report Format Defined in the below section

REPORT Report_Emp(m)
    DEFINE m RECORD
        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,
        --expense_id1 LIKE expense_details.expense_id,
        date LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        miles_total DECIMAL(9,2),
        total_charged_to_fourjs DECIMAL(9,2),
        total_paid_by_employee DECIMAL(9,2),

        date1 LIKE business_details.date,
        amount1 LIKE business_details.amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans,
         Entertainment VARCHAR(50),
        Meals VARCHAR(50),
        Lodging VARCHAR(50),
        Autorent VARCHAR(50),
        Fuel VARCHAR(50),
        Transport VARCHAR(50),
        Tolls VARCHAR(50),
        Miles VARCHAR(50),
        Phone VARCHAR(50),
        Others VARCHAR(50),
        Airfare VARCHAR(50)

    END RECORD
   -- DEFINE g_total DECIMAL(9,2)
    
    ORDER EXTERNAL BY m.exp_id

    FORMAT
        FIRST PAGE HEADER
        PAGE HEADER

            PRINTX today_date
        ON EVERY ROW
            LET today_date = DATE(TODAY)
            LET m.date = today_date
            DISPLAY "Today Date,", today_date
            PRINTX today_date
            LET today_date = DATE(TODAY)
            LET m.date1 = today_date
            --DISPLAY "myrec.*",myrec.expense_id
            --LET g_total = m.charge_to_fourjs + m.total_charged_to_fourjs + m.total_paid_by_employee
            PRINTX m.emp_no,
                m.emp_name,
                m.position,
                m.manager,
                m.department,
                m.notes,
                m.charge_type,
                m.date,
                m.location,
                m.expense_type,
                m.mileage,
                m.amount,
                m.charge_to_fourjs,
                m.miles_total,
                m.total_charged_to_fourjs,
                m.total_paid_by_employee,
                m.date,
                m.amount1,
                m.company_name,
                m.business_trans,
                m.Entertainment,
                m.Meals,
                m.Lodging,
                m.Autorent,
                m.Fuel,
                m.Transport,
                m.Tolls,
                m.Phone,
                m.Others,
                m.Airfare,
               -- g_total,
                SUM(m.total_charged_to_fourjs)
            PRINTX rpt,
        
                "-------------------------END REPORT------------------------"

        AFTER GROUP OF m.exp_id
            PRINTX m.exp_id, "FORM RECORDS"
            PRINTX totalp
            PRINTX totalc
            
            SKIP 2 LINES

END REPORT